<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'HomeCtrl@index')->name('home');
Route::get('contact', 'HomeCtrl@showContactPage')->name('contact');
Route::get('courses', 'HomeCtrl@showCoursesPage')->name('courses');
Route::get('diplomas', 'HomeCtrl@showDiplomasPage')->name('diplomas');
Route::get('blog', 'HomeCtrl@showBlogPage')->name('blog');
Route::group(['middleware' => 'auth'], function() {
    //
    Route::get('profile', 'AccountCtrl@showProfilePage')->name('profile');
    Route::get('certificates/download/{id}', 'CertificateCtrl@downloadCertificate')->name('download-certificate');

    Route::group(['middleware' => 'admin'], function(){
        Route::get('dashboard', 'DashboardCtrl@index')->name('dashboard');

        /* User Module */
        Route::get('users/all', 'UserCtrl@index')->name('all-users');
        Route::get('users/edit/{id}', 'UserCtrl@editUser')->name('edit-user');
        Route::post('save-user', 'UserCtrl@saveUser');
        Route::get('users/delete', 'UserCtrl@deleteUser');
        Route::get('users/show/{id}', 'UserCtrl@showUser')->name('show-user');

        /* Student Module */
        Route::get('students/all', 'StudentCtrl@index')->name('all-students');
        Route::get('students/create', 'StudentCtrl@createStudent')->name('create-student');
        Route::post('store-student', 'StudentCtrl@storeStudent');
        Route::get('students/edit/{id}', 'StudentCtrl@editStudent')->name('edit-student');
        Route::post('save-student', 'StudentCtrl@saveStudent');
        Route::get('students/delete', 'StudentCtrl@deleteStudent');

        /* Course Module */
        Route::get('courses/all', 'CoursesCtrl@index')->name('all-courses');
        Route::get('courses/create', 'CoursesCtrl@createCourse')->name('create-course');
        Route::post('store-course', 'CoursesCtrl@storeCourse');
        Route::get('courses/edit/{id}', 'CoursesCtrl@editCourse')->name('edit-course');
        Route::post('save-course', 'CoursesCtrl@saveCourse');
        Route::get('courses/delete', 'CoursesCtrl@deleteCourse');

        /* Diploma Module */
        Route::get('diplomas/all', 'DiplomaCtrl@index')->name('all-diplomas');
        Route::get('diplomas/create', 'DiplomaCtrl@createDiploma')->name('create-diploma');
        Route::post('store-diploma', 'DiplomaCtrl@storeDiploma');
        Route::get('diplomas/edit/{id}', 'DiplomaCtrl@editDiploma')->name('edit-diploma');
        Route::post('save-diploma', 'DiplomaCtrl@saveDiploma');
        Route::get('diplomas/delete', 'DiplomaCtrl@deleteDiploma');

        /* Certificate Module */
        Route::get('certificates/all', 'CertificateCtrl@index')->name('all-certificates');
        Route::get('certificates/create', 'CertificateCtrl@createCertificate')->name('create-certificate');
        Route::post('store-certificate', 'CertificateCtrl@storeCertificate');
        Route::get('certificates/edit/{id}', 'CertificateCtrl@editCertificate')->name('edit-certificate');
        Route::post('save-certificate', 'CertificateCtrl@saveCertificate');
        Route::get('certificates/delete', 'CertificateCtrl@deleteCertificate');

      });
});