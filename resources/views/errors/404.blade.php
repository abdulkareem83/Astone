<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>University || 404 Pages</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

		<!-- all css here -->
		<!-- bootstrap v3.3.7 css -->
        <link rel="stylesheet" href="{{ url('css')}}/bootstrap.min.css">
		<!-- animate css -->
        <link rel="stylesheet" href="{{ url('css')}}/animate.css">
		<!-- owl.carousel.2.0.0-beta.2.4 css -->
        <link rel="stylesheet" href="{{ url('css')}}/owl.carousel.css">
		<!-- slicknav.min.css -->
        <link rel="stylesheet" href="{{ url('css')}}/slicknav.min.css">
		<!-- font-awesome v4.6.3 css -->
        <link rel="stylesheet" href="{{ url('css')}}/font-awesome.min.css">
		<!-- video-js.css -->
        <link rel="stylesheet" href="{{ url('css')}}/video-js.css">
		<!-- style css -->
		<link rel="stylesheet" href="{{ url('css')}}/style.css">
		<!-- responsive css -->
        <link rel="stylesheet" href="{{ url('css')}}/responsive.css">
		<!-- modernizr css -->
        <script src="{{ url('js') }}/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<div class="area-404">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<h2>Error Page!</h2>
					</div>
					<div class="col-md-6 text-right col-sm-6 col-xs-12">
						<h2><a href="{{ url('') }}"><i class="fa fa-long-arrow-left"></i>Back To Home</a></h2>
					</div>
					<div class="col-xs-12 text-center">
						<img src="{{ url('img') }}/404.png" alt="" />
					</div>
					<div class="col-md-6 col-sm-3 col-xs-12">
						<ul class="socil-icon">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
					<div class="col-md-6 text-right col-sm-9 col-xs-12">
						<p>&copy; COPYRIGHT Astone Uk Online {{ date("Y") }}.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- all js here -->
		<!-- jquery latest version -->
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
		<!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
		<!-- owl.carousel.2.0.0-beta.2.4 css -->
        <script src="js/owl.carousel.min.js"></script>
		<!-- wow js -->
        <script src="js/video.js"></script>
		<!-- jquery.slicknav.min.js -->
        <script src="js/jquery.slicknav.min.js"></script>
		<!-- jquery.waypoints.min.js -->
        <script src="js/jquery.waypoints.min.js"></script>
		<!-- counterup.main.js -->
        <script src="js/counterup.main.js"></script>
		<!-- wow js -->
        <script src="js/videojs-ie8.min.js"></script>
		<!-- wow js -->
        <script src="js/wow.min.js"></script>
		<!-- plugins js -->
        <script src="js/plugins.js"></script>
		<!-- main js -->
        <script src="js/main.js"></script>
    </body>
</html>