<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Astone || {{ $title ? $title : 'University' }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="{{url('css')}}/bootstrap.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="{{url('css')}}/animate.css">
    <!-- owl.carousel.2.0.0-beta.2.4 css -->
    <link rel="stylesheet" href="{{url('css')}}/owl.carousel.css">
    <!-- slicknav.min.css -->
    <link rel="stylesheet" href="{{url('css')}}/slicknav.min.css">
    <!-- font-awesome v4.6.3 css -->
    <link rel="stylesheet" href="{{url('css')}}/font-awesome.min.css">
    <!-- video-js.css -->
    <link rel="stylesheet" href="{{url('css')}}/video-js.css">
    <!-- style css -->
    <link rel="stylesheet" href="{{url('css')}}/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{url('css')}}/responsive.css">
    <!-- modernizr css -->
    <script src="{{url('js')}}/vendor/modernizr-2.8.3.min.js"></script>
  </head>
  <body>
    <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- header area start -->
    <header class="{{ isset($indexPage) ? 'header-area': '' }}">
      <div class="header-top {{ isset($indexPage) ? '': 'bg-1' }} ptb-20">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-6 {{ isset($indexPage) ? '': 'col-xs-12' }}">
              <div class="header-top-left">
                <ul class="socil-icon">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 {{ isset($indexPage) ? '': 'col-xs-12' }}">
              <div class="header-top-right text-right">
                <ul>
                  @unless (Auth::user())
                    <li><a href="{{ url('login') }}">Log in <i class="fa fa-user"></i></a></li>
                    <li><a href="{{ url('register') }}"><i class="fa fa-unlock-alt"></i> Register</a></li>
                  @else
                    @if(Auth::user()->role == 'admin')
                      <li>
                        <a href="{{ url('dashboard') }}" target="__blank"><i class="fa fa-dashboard"></i> Admin  Dashboard</a>
                      </li>
                    @endif
                    <li><a href="#">{{ Auth::User()->name }} <i class="fa fa-user"></i></a></li>
                    <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
                  @endunless

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="header-bottom {{ isset($indexPage) ? 'home2-header-bottom': '' }}" id="sticky-header">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-sm-3 col-xs-8">
              <div class="logo">
                <a href="{{ url('') }}">
                  <img src="{{ url('img') }}/astone-logo.png" width="217" height="54" alt="" />
                </a>
              </div>
            </div>
            <div class="col-md-8 col-sm-9 hidden-xs">
              <div class="mainmenu text-right">
                <ul id="navigation">
                  <li class="active"><a href="{{ url('') }}">Home</a>
                  </li>
                  <li>
                    <a href="{{ url('courses') }}">Courses</a>
                  </li>
                  <li>
                    <a href="{{ url('diplomas') }}">Diplomas</a>
                  </li>
                  <li><a href="{{ url('contact') }}">contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-xs-4 hidden-lg hidden-md hidden-sm">
              <div class="responsive-menu-wrap floatright">
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- header area end -->    
    @yield('content')
    <!-- footer-area start -->
      <!-- footer-area start -->
      <footer>
        <div class="footer-top ptb-120">
          <div class="container">
            <div class="row">
              <div class="col-sm-4 col-xs-12">
                <div class=" footer-widget">
                  <img src="{{ url('img') }}/astone-logo.png" width="217" height="54" alt="" />
                  <div class="socil-icon">
                    <ul>
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                      <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                      <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-sm-4 col-xs-12">
                <div class="footer-widget footer-menu">
                  <h2>Course</h2>
                  <ul>
                    @if(isset($featuredCourses))
                      @foreach($featuredCourses as $featuredCourse)
                        <li>
                          <a href="#">{{ $featuredCourse->name }}</a>
                        </li>
                      @endforeach
                    @else
                      There is not courses
                    @endif
                  </ul>
                </div>
              </div>
              <div class="col-sm-4 col-xs-12">
                <div class="footer-widget footer-menu">
                  <h2>Diplomas</h2>
                  <ul>
                    <ul>
                      @if(isset($featuredDiplomas))
                        @foreach($featuredDiplomas as $featuredDiplomas)
                          <li>
                            <a href="#">{{ $featuredDiplomas->name }}</a>
                          </li>
                        @endforeach
                      @else
                        There is not diplomas
                      @endif
                    </ul>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-bottom">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="copyright">
                  &copy; COPYRIGHT Aston Uk . DESIGN BY <a href="https://www.linkedin.com/in/abdul-kareem-mohammed-8aa3b9b5">Abdulkareem Mohammed</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- footer-area end -->
      
      <!-- all js here -->
      <!-- jquery latest version -->
          <script src="js/vendor/jquery-1.12.4.min.js"></script>
      <!-- bootstrap js -->
          <script src="js/bootstrap.min.js"></script>
      <!-- owl.carousel.2.0.0-beta.2.4 css -->
          <script src="js/owl.carousel.min.js"></script>
      <!-- wow js -->
          <script src="js/video.js"></script>
      <!-- jquery.slicknav.min.js -->
          <script src="js/jquery.slicknav.min.js"></script>
      <!-- jquery.waypoints.min.js -->
          <script src="js/jquery.waypoints.min.js"></script>
      <!-- counterup.main.js -->
          <script src="js/counterup.main.js"></script>
      <!-- wow js -->
          <script src="js/videojs-ie8.min.js"></script>
      <!-- wow js -->
          <script src="js/wow.min.js"></script>
      <!-- plugins js -->
          <script src="js/plugins.js"></script>
      <!-- main js -->
          <script src="js/main.js"></script>
          @yield('script')
      </body>
  </html>
