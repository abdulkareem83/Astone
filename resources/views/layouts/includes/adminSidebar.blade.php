<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url('admin') }}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="{{ Request::url() == route('all-users') ? 'active' : '' }}">
          <a href="{{ route('all-users') }}"><i class="fa fa-users"></i>All Users</a>
        </li>
        <li class="treeview {{ Request::segment(1) == 'students' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa fa-user"></i>
            <span>Students</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::url() == route('all-students') ? 'active' : '' }}">
              <a href="{{ route('all-students') }}"><i class="fa fa-circle-o"></i>All Students</a>
            </li>
            <li class="{{ Request::url() == route('create-student') ? 'active' : '' }}">
              <a href="{{ route('create-student') }}"><i class="fa fa-circle-o"></i>Add new student</a>
            </li>
          </ul>
        </li>
        <li class="treeview {{ Request::segment(1) == 'courses' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa fa-laptop"></i>
            <span>Courses</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::url() == route('all-courses') ? 'active' : '' }}">
              <a href="{{ route('all-courses') }}"><i class="fa fa-circle-o"></i>All Courses</a>
            </li>
            <li class="{{ Request::url() == route('create-course') ? 'active' : '' }}">
              <a href="{{ route('create-course') }}"><i class="fa fa-circle-o"></i>Add new course</a>
            </li>
          </ul>
        </li>
        <li class="treeview {{ Request::segment(1) == 'diplomas' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa fa-laptop"></i>
            <span>Diplomas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::url() == route('all-diplomas') ? 'active' : '' }}">
            	<a href="{{ route('all-diplomas') }}"><i class="fa fa-circle-o"></i>All Diplomas</a>
            </li>
            <li class="{{ Request::url() == route('create-diploma') ? 'active' : '' }}">
            	<a href="{{ route('create-diploma') }}"><i class="fa fa-circle-o"></i>Add new diploma</a>
            </li>
          </ul>
        </li>
        <li class="treeview {{ Request::segment(1) == 'certificates' ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Certificates</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::url() == route('all-certificates') ? 'active' : '' }}">
              <a href="{{ route('all-certificates') }}"><i class="fa fa-circle-o"></i>All Certificates</a>
            </li>
            <li class="{{ Request::url() == route('create-certificate') ? 'active' : '' }}">
              <a href="{{ route('create-certificate') }}"><i class="fa fa-circle-o"></i>Add new certificate</a>
            </li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>