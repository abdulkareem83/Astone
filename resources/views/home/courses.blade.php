@extends("layouts.homeLayout")

@section('content')
	<div class="course-area ptb-120 bg-2">
		<div class="container">
			<div class="row">
				<div class="course-list-active">
					<div class="col-xs-12">
						@if (count($courses))
							@foreach($courses as $course)
								<div class="course-item clear">
									<div class="course-img">
										<img src="{{ $course->picture }}" alt="" />
									</div>
									<div class="course-content">
										<h3><a href="#">{{ $course->name }}</a></h3>

										<p>
											{{ $course->description }}
										</p>
									</div>
								</div>
							@endforeach
						@else
							There is no courses yet.
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@stop