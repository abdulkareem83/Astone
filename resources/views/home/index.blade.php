@extends('layouts.homeLayout')

@section('content')
  <!-- header area end -->
    <!-- slider-area start -->
  <div class="slider-area home2-slider-area">
    <div class="slider-active">
      <div class="single-slider opacity-background">
        <img src="img/slider/home2/1.jpg" alt="" />
        <div class="slider-content text-center">
          <div class="table">
            <div class="table-cell">
              <div class="container">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                    <h2>Better Education For a Better World</h2>
                    <p>Almost before we knew it we had left the ground.The face of the moon was in shadow.The spectacle before us was indeed sublime.All their equipment and instruments are alive.All their equipment and instruments are alive.It watched the storm, so beautiful yet terrific.</p>
                    <a href="#">Apply now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="single-slider opacity-background">
        <img src="img/slider/home2/2.jpg" alt="" />
        <div class="slider-content text-center">
          <div class="table">
            <div class="table-cell">
              <div class="container">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                    <h2 class="slide-left">Better Education For a Better World</h2>
                    <p>Almost before we knew it we had left the ground.The face of the moon was in shadow.The spectacle before us was indeed sublime.All their equipment and instruments are alive.All their equipment and instruments are alive.It watched the storm, so beautiful yet terrific.</p>
                    <a href="#">Apply now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="single-slider opacity-background">
        <img src="img/slider/home2/3.jpg" alt="" />
        <div class="slider-content text-center">
          <div class="table">
            <div class="table-cell">
              <div class="container">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                    <h2 class="slide-right">Better Education For a Better World</h2>
                    <p>Almost before we knew it we had left the ground.The face of the moon was in shadow.The spectacle before us was indeed sublime.All their equipment and instruments are alive.All their equipment and instruments are alive.It watched the storm, so beautiful yet terrific.</p>
                    <a href="#">Apply now</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- slider-area end -->

  <div class="home2-course-area bg-2">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="home2-course-sidebar-area">
            <div class="course-sidebar-wrap">
                <div class="row">
                  <div class="col-md-4 col-xs-12">
                    <div class="course-sidebar-menu">
                      <ul>
                        <li class="active">
                          <a href="#details" data-toggle="tab">
                            CERTIFICATE DETAILS
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div class="upload-wrap text-center">
                        <label for="imageUpload"><i class="fa fa-search"></i></label>
                        <p clickable="false">Check The  Certificate</p>
                    </div>
                  </div>
                  <div class="col-md-8 col-xs-12">
                    <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="details">
                        @if (session("validSerialNumber"))
                          <div class="row">
                            <div class="col-md-6">
                              <div class="course-from">
                                <form action="" method="get">
                                  <ul>
                                    <li>
                                      <label>Select Certificate Type<span>*</span></label>
                                          <span class="radio-inline">
                                            <input type="radio" name="certificate_type" value="course" checked=""> Course
                                          </span>
                                          <span class="radio-inline">
                                            <input type="radio" name="certificate_type" value="diploma"> Diploma
                                          </span>
                                    </li>
                                    <li class="course target">
                                      <label>Select Course<span>*</span></label>
                                        @if (count($items["courses"]))
                                          <select name="courses" class="select-certificate">
                                            <option value="0">Select your certificate</option>
                                          @foreach($items["courses"] as $course)
                                            <option value="{{ $course->certificateId }}">
                                              {{ $course->name }}
                                            </option>
                                          @endforeach
                                          </select>
                                        @else
                                          <input type="text" readonly placeholder="You haven't any courses certificates" />
                                        @endif
                                    </li>
                                    <li class="hidden diploma target">
                                      <label>Select Diploma<span>*</span></label>
                                      @if (count($items["diplomas"]))
                                        <select name="diplomas" class="select-certificate">
                                          <option value="0">Select your certificate</option>
                                        @foreach($items["diplomas"] as $diploma)
                                          <option value="{{ $diploma->certificateId }}" >
                                            {{ $diploma->name }}
                                          </option>
                                        @endforeach
                                        </select>
                                      @else
                                        <input type="text" readonly placeholder="You haven't any diplomas certificates" />
                                      @endif
                                    </li>
                                  </ul>
                                </form>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="course-from">
                                  <ul>
                                    <li>
                                      <label>DESCRIPTION <span>*</span></label>
                                      <p class="description">
                                          Please select a certificate to download.
                                        
                                      </p>
                                    </li>
                                  </ul>
                                  <div class="next-page text-center mt10">
                                    
                                    <button id="download-certificate" type="button" class="astone-button download" data-toggle="modal" data-target="#certificateModal">
                                      Download
                                      <i class="fa fa-arrow-circle-o-down"></i>
                                    </button>
                                    <a href="{{ url('?rsn=1')}}" class="btn btn-sm btn-danger re-insert">
                                          <i class="fa fa-undo"> </i> Reinsert Id
                                        </a>
                                  </div>
                              </div>
                            </div>
                          </div>
                        @else
                          <div class="row">
                            <div class="col-md-12">
                              <div class="course-from mt-63">
                                <form action="" method="get">
                                  <ul>
                                    <li> 
                                      <label>Please Insert Your National Id <span>*</span></label>
                                      <input type="text" name="sn" placeholder="National Id" required/>
                                      <span class="help-block invalid-sn">
                                        {{ session('invalidSN') ? session('invalidSN') : '' }}
                                      </span>
                                      <p class="text-center">
                                        <button class="astone-button">
                                          Check Certificate
                                        </button>
                                      </p>
                                    </li>
                                  </ul>
                                </form>
                              </div>
                            </div>
                          </div>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <!-- latest news -->
          <div class="latest-news-wrap">
            <h2 class="news-title">LATEST NEWS</h2>
            <div class="latest-news">
              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <ul>
                    <li>
                      <span><i class="fa fa-clock-o"></i> December 15, 2017</span>
                      <p><a href="#">The face of the moon was in shadow.so beautiful yet terrific.</a></p>
                    </li>
                    <li>
                      <span><i class="fa fa-clock-o"></i> December 20, 2017</span>
                      <p><a href="#">Almost before we knew it, we had left the ground.It was going to.</a></p>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4 col-xs-12">
                  <ul>
                    <li>
                      <span><i class="fa fa-clock-o"></i> December 15, 2017</span>
                      <p><a href="#">The face of the moon was in shadow.so beautiful yet terrific.</a></p>
                    </li>
                    <li>
                      <span><i class="fa fa-clock-o"></i> December 20, 2017</span>
                      <p><a href="#">Almost before we knew it, we had left the ground.It was going to.</a></p>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4 col-xs-12">
                  <ul>
                    <li>
                      <span><i class="fa fa-clock-o"></i> December 15, 2017</span>
                      <p><a href="#">The face of the moon was in shadow.so beautiful yet terrific.</a></p>
                    </li>
                    <li>
                      <span><i class="fa fa-clock-o"></i> December 20, 2017</span>
                      <p><a href="#">Almost before we knew it, we had left the ground.It was going to.</a></p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!-- latest news -->
        </div>
      </div>
      <div class="course-wrap ptb-120">
        <h2 class="section-title">POPULAR COURSES</h2>
        <div class="row">
          <div class="course-active2">
            @if (count($featuredCourses))
              @foreach($featuredCourses as $featuredCourse)
                <div class="col-xs-12">
                  <div class="course-item">
                    <div class="course-img">
                      <img src="{{ $featuredCourse->picture }}" alt="" />
                    </div>
                    <div class="course-content">
                      <h3><a href="#">{{ $featuredCourse->name }}</a></h3>
                      <div class="course-meta"></div>
                      <p>
                        {{ $featuredCourse->description }}
                      </p>
                    </div>
                  </div>
                </div>
              @endforeach
            @else
              There is no courses yet
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- event-area start -->
  <div class="event-area ptb-120">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h2 class="section-title">Diplomas</h2>
          <div class="event-wrap">
            <ul>
              @if (count($featuredDiplomas))
              @foreach($featuredDiplomas as $key => $featuredDiploma)
                <li>
                  @if ($key % 2 )
                    <div class="event-time width-one"></div>
                  @else
                    <div class="event-img width-two">
                      <img src="{{ $featuredDiploma->picture }}" alt="" />
                    </div>
                  @endif
                  <div class="event-content width-three">
                    <h3>
                      <a href="#">
                        {{ $featuredDiploma->name }}
                      </a>
                    </h3>
                    <p>
                      {{ str_limit($featuredDiploma->description, 340)  }}
                    </p>
                  </div>
                  @if ($key % 2 )
                    <div class="event-img width-two">
                      <img height="50" src="{{ $featuredDiploma->picture }}" alt="" />
                    </div>
                  @else
                    <div class="event-time width-one event-time2"></div>
                  @endif
                </li>
              @endforeach
              @else
                There is no diplomas yet.
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- event-area end -->

<!-- Modal -->
<div class="modal fade bs-example-modal-lg modal-info" id="certificateModal" tabindex="-1" role="dialog" aria-labelledby="certificateModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="certificateModalLabel">Certificates</h4>
      </div>
      <div class="modal-body">
        <div id="certificateTab" class="hidden">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active pictureListItem"><a href="#picture" aria-controls="picture" role="tab" data-toggle="tab">Certificate Picture</a></li>
            <li role="presentation"><a href="#detailsPicture" aria-controls="detailsPicture" role="tab" data-toggle="tab">Certificate Details Picture</a></li>
          </ul> 

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="picture">There is no image to download</div>
            <div role="tabpanel" class="tab-pane" id="detailsPicture">There is no image to download</div>
          </div>
        </div>
        <div class="invalid">
          <p>
            There is no images to print!
          </p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@stop

@section('script')
  <script type="text/javascript">
        var items = JSON.parse('{!! json_encode($items) !!}');
      $(document).ready(function(){

          $('body').on('change', '.select-certificate', function(){
            var description = "";
            var id = $(this).val();
            var name = $(this).attr("name");

            if(items[name]["course"+id] != undefined)
              description = items[name]["course"+id]["description"];
            if(description.length > 230) description = description.substring(0,230);
            $('.course-from .description').text(description + "...");
          });

          @if (Request::has('sn'))
          $('html, body').animate({scrollTop: $("div.home2-course-sidebar-area").offset().top   }, 2000);
          @endif

          $('body').on("change", "input[name=certificate_type]", function(){
            var type = $(this).val();
            if(type == "course"){
              $("li.target.course").removeClass("hidden");
              $("li.target.diploma").addClass("hidden");
            }else{
              $("li.target.diploma").removeClass("hidden");
              $("li.target.course").addClass("hidden");
            }  
          })

          $('body').on("click", "#download-certificate", function(){
              $modalBody = $("#certificateModal").find(".modal-body");
              $modalBody.find("#certificateTab").addClass("hidden");
              $modalBody.find(".invalid").removeClass("hidden");

              var selectList = $("li.target").not(".hidden").find("select");
              if(selectList != undefined){
                var certificateId = selectList.val();
                if( certificateId != undefined && certificateId != 0){
                  var name = selectList.attr("name");
                  if(items[name]["course"+certificateId] != undefined){
                    var item = items[name]["course"+certificateId];
                    var pictureBody = detailsPictureBody = "<p>Ther is no image to print!</p>";
                    
                    if(item['details_picture'] != '')
                      detailsPictureBody = "<img src='"+item['details_picture']+"' />";
                    if(item['picture'] != '')
                      pictureBody = "<img src='"+item['picture']+"' />";

                      $certificateTab = $("#certificateTab");
                      $certificateTab.find('#picture').html(pictureBody);
                      $certificateTab.find('#detailsPicture').html(detailsPictureBody);
                      $(".pictureListItem a").trigger('click');
                      $modalBody.find("#certificateTab").removeClass("hidden");
                      $modalBody.find(".invalid").addClass("hidden");
                  }
                }
              }
              
          })
      });
  </script>
@stop