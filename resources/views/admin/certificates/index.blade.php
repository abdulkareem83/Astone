@extends('layouts.adminLayout')

@section('content')
	<section class="all-certificates-section">
		<div class="box box-info">
			<div class="box-header with-border">
				{{ $title }}
			</div>
			<div class="box-body">
				@if (count($certificates))
					<div class="table-responsive">
						<table class="table table-bordered table-stripped">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>User</th>
									<th class="mw125">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($certificates as $key => $certificate)
								<tr>
									<td>
										{{ $key + 1 }}
									</td>
									<td>
										{{ $certificate->name }}
									</td>
									<td> {{ $certificate->student->name }} </td>
									<td>
										<div class="btn-group">
											<a title="Edit" class="btn btn-info btn-sm" href="{{ route('edit-certificate', ['id' => $certificate->id]) }}"><i class="fa fa-edit"></i></a>
											<a title="Delete" class="btn btn-danger btn-sm" href='{{ url("certificates/delete?id={$certificate->id}&token=".csrf_token()) }}'><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@else
					There is No records yet.
				@endif
			</div>
		</div>
	</section>
@stop