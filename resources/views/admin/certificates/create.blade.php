@extends('layouts.adminLayout')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ url('admin') }}/plugins/bootsnipp-file-input/bootsnipp-file-input.css">
	<link rel="stylesheet" href="{{ url('admin') }}/bower_components/select2/dist/css/select2.min.css">
@stop

@section('content')
	<section class="create-certificate-section">
		<div class="box box-info">
			<div class="box-header with-border">
				{{ $title }}
			</div>
			<div class="box-body">
				<form role="form" enctype="multipart/form-data" action="{{ isset($certificate) ? url('save-certificate') : url('store-certificate') }}" method="post">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>
							Name
						</label>
						{{ csrf_field() }}
						@if (isset($certificate))
							<input type="hidden" name="id" value="{{ $certificate->id }}">
						@endif
						<input type="text" required="" class="form-control" name="name" value="{{ isset($certificate) ? $certificate->name : old('name') }}">
						@if ($errors->has('name'))
							<span class='help-block'>
								{{ $errors->first('name') }}
							</span>
						@endif
					</div>
					<div class="form-group {{ $errors->has('student_id') ? 'has-error' : '' }}">
						<label>
							Student
						</label>
						@if (count($students))
							<select name="student_id" class="form-control select2" required="">
								@foreach( $students as $student )
									<option value="{{ $student->id }}" {{ isset($certificate) ? ($student->id == $certificate->student_id? 'selected': '') : (Request::input('student_id') == $student->id ? 'selected' : '')  }}>
										{{ $student->name ." - " . $student->serial_number }}
									</option>
								@endforeach
							</select>
						@endif
						@if ($errors->has('student_id'))
							<span class='help-block'>
								{{ $errors->first('student_id') }}
							</span>
						@endif
					</div>
					<div class="form-group {{ $errors->has('course_id') ? 'has-error' : '' }}">
						<label>
							Course
						</label>
						@if (count($courses))
							<select required="" name="course_id" class="form-control select2">
								@foreach( $courses as $course )
									<option value="{{ $course->id }}" {{ isset($certificate) ? ($course->id == $certificate->course_id? 'selected': '') : ''  }}>
										{{ $course->name ." - " . $course->email }}
									</option>
								@endforeach
							</select>
						@endif
						@if ($errors->has('course_id'))
							<span class='help-block'>
								{{ $errors->first('course_id') }}
							</span>
						@endif
					</div>
					<div class="form-group has-feedback {{ $errors->has( 'picture' ) ? 'has-error' : '' }}">
					    <label class="image-label">
					        Picture
					    </label>
					    @if( isset( $certificate ) )
					        <div class="icon-container">
					            <p href="#" class="thumbnail">
					                <img class="responsive-img" src="{{ $certificate->picture }}" alt="{{ trans('lang.picture') }}">
					            </p>
					        </div>
					    @endif
					    <!-- image-preview-filename input [CUT FROM HERE]-->
					    <div class="input-group image-preview">
					        <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
					        <span class="input-group-btn">
					            <!-- image-preview-clear button -->
					            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
					                <span class="glyphicon glyphicon-remove"></span> Delete Image
					            </button>
					            <!-- image-preview-input -->
					            <div class="btn btn-default image-preview-input">
					                <span class="glyphicon glyphicon-folder-open"></span>
					                <span class="image-preview-input-title">Browse</span>
					                <input type="file" accept="image/png, image/jpeg, image/gif" name="picture" {{ isset($certificate) ? '' : 'required' }} /> <!-- rename it -->
					            </div>
					        </span>
					    </div><!-- /input-group image-preview [TO HERE]--> 
					    <span class="help-block">
					        {{ $errors->has( 'picture' ) ? $errors->first( 'picture' ) : '' }}
					    </span>
					</div>
					<div class="form-group has-feedback {{ $errors->has( 'details_picture' ) ? 'has-error' : '' }}">
					    <label class="image-label">
					        Details picture
					    </label>
					    @if( isset( $certificate ) )
					        <div class="icon-container">
					            <p href="#" class="thumbnail">
					                <img class="responsive-img" src="{{ $certificate->details_picture }}" alt="{{ trans('lang.details_picture') }}">
					            </p>
					        </div>
					    @endif
					    <div class="input-group">
					        <input type="file" name="details_picture" id="" class="form-control">
					        <span class="input-group-addon" for>
					        	Upload file
					        </span>
					    </div><!-- /input-group image-preview [TO HERE]--> 
					    <span class="help-block">
					        {{ $errors->has( 'details_picture' ) ? $errors->first( 'details_picture' ) : '' }}
					    </span>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-sm btn-info pull-right">{{ isset($certificate) ? 'Save' : 'Create' }}</button>
					</div>
				</form>
			</div>
		</div>
	</section>
@stop

@section('script')
	<script type="text/javascript" src="{{ url('admin') }}/plugins/bootsnipp-file-input/bootsnipp-file-input.js"></script>
	<!-- Select2 -->
	<script src="{{ url('admin') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.select2').select2();
		});
	</script>
@stop