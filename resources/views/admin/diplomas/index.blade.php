@extends('layouts.adminLayout')

@section('content')
	<section class="all-courses-section">
		<div class="box box-info">
			<div class="box-header with-border">
				{{ $title }}
			</div>
			<div class="box-body">
				@if (count($diplomas))
					<div class="table-responsive">
						<table class="table table-bordered table-stripped">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Description</th>
									<th class="mw125">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($diplomas as $key => $diploma)
								<tr>
									<td>
										{{ $key + 1 }}
									</td>
									<td>
										{{ $diploma->name }}
									</td>
									<td> {{ str_limit($diploma->description, 200) }} </td>
									<td>
										<div class="btn-group">
											<a title="Edit" class="btn btn-info btn-sm" href="{{ route('edit-diploma', ['id' => $diploma->id]) }}"><i class="fa fa-edit"></i></a>
											<a title="Delete" class="btn btn-danger btn-sm" href='{{ url("diplomas/delete?id={$diploma->id}&token=".csrf_token()) }}'><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@else
					There is No records yet.
				@endif
			</div>
		</div>
	</section>
@stop