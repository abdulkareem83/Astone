@extends('layouts.adminLayout')

@section('content')
	<section class="all-courses-section">
		<div class="box box-info">
			<div class="box-header with-border">
				All users
			</div>
			<div class="box-body">
				@if (count($users))
					<div class="table-responsive">
						<table class="table table-bordered table-stripped">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>User name</th>
									<th>Role</th>
									<th class="mw125">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($users as $key => $user)
								<tr>
									<td>
										{{ $key + 1 }}
									</td>
									<td>
										{{ $user->name }}
									</td>
									<td>
										{{ $user->email }} 
									</td>
									<td>
										{{ ucfirst($user->role) }}
									</td>
									<td>
										<div class="btn-group">
											<a title="Edit" class="btn btn-info btn-sm" href="{{ route('edit-user', ['id' => $user->id]) }}"><i class="fa fa-edit"></i></a>
											<a title="Delete" class="btn btn-danger btn-sm" href='{{ url("users/delete?id={$user->id}&token=".csrf_token()) }}'><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@else
					{{ trans('lang.therIsNoRecords') }}
				@endif
			</div>
		</div>
	</section>
@stop