@extends('layouts.adminLayout')

@section('head')
	<link rel="stylesheet" href="{{ url('admin') }}/bower_components/select2/dist/css/select2.min.css">
@stop

@section('content')
	<section class="edit-user-section">
		<div class="box box-info">
			<div class="box-header with-border">
				Edit User
			</div>
			<div class="box-body">
				<form class="form-horizontal" role="form" enctype="multipart/form-data" action="{{ url('save-user')}}" method="post">
					<div class="form-group">
						<label class="col-md-3">
							Name
						</label>
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $user->id }}">
						<div class="col-md-9">
							<input type="text" readonly class="form-control" value="{{ $user->name }}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">
							Username
						</label>
						<div class="col-md-9">
							<input type="text" readonly class="form-control"  value="{{ $user->email }}">
						</div>
					</div>
					<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
						<label class="col-md-3">
							Password
						</label>
						{{ csrf_field() }}
						<input type="hidden" name="id" value="{{ $user->id }}">
						<div class="col-md-9">
							<input type="password" class="form-control" name="password">
							<span class="help-block">
							    {{ $errors->has( 'password' ) ? $errors->first( 'password' ) : '' }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
						<label class="col-md-3">
							Confirm Password
						</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="password_confirmation">
							<span class="help-block">
							    {{ $errors->has( 'password' ) ? $errors->first( 'password' ) : '' }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('role') ? 'has-error' : '' }}">
						<label class="col-md-3">
							Role
						</label>
						<div class="col-md-9">
							<select class="form-control" name="role">
								<option value="student" {{ $user->role == 'student' ? 'selected' : '' }}>Student</option>
								<option value="admin" {{ $user->role == 'admin' ? 'selected' : '' }}>Admin</option>
							</select>
							<span class="help-block">
							    {{ $errors->has( 'role' ) ? $errors->first( 'role' ) : '' }}
							</span>
						</div>
					</div>
					<div>
						<button type="submit" class="btn btn-sm btn-info pull-right">Save</button>
						<div class="clearfix" />
					</div>
				</form>
			</div>
		</div>
	</section>
@stop

@section('script')
	<!-- Select2 -->
	<script src="{{ url('admin') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.select2').select2();
		});
	</script>
@stop