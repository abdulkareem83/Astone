@extends('layouts.adminLayout')

@section('content')
	<section class="all-students-section">
		<div class="box box-info">
			<div class="box-header with-border">
				{{ $title }}
			</div>
			<div class="box-body">
				@if (count($students))
					<div class="table-responsive">
						<table class="table table-bordered table-stripped">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Serial Number</th>
									<th class="mw125">
										Actions
									</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($students as $key => $student)
								<tr>
									<td>
										{{ $key + 1 }}
									</td>
									<td>
										{{ $student->name }}
									</td>
									<td> {{ $student->serial_number }} </td>
									<td>
										<div class="btn-group">
											<a title="Edit" class="btn btn-info btn-sm" href="{{ route('edit-student', ['id' => $student->id]) }}"><i class="fa fa-edit"></i></a>
											<a title="Add Student to course" class="btn btn-success btn-sm" href="{{ route('create-certificate', ['student-id' => $student->id]) }}"><i class="fa fa-edit"></i></a>
											<a title="Delete" class="btn btn-danger btn-sm" href='{{ url("students/delete?id={$student->id}&token=".csrf_token()) }}'><i class="fa fa-trash"></i></a>
										</div>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				@else
					There is No records yet.
				@endif
			</div>
		</div>
	</section>
@stop