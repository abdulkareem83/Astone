@extends('layouts.adminLayout')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ url('admin') }}/plugins/bootsnipp-file-input/bootsnipp-file-input.css">
	<link rel="stylesheet" href="{{ url('admin') }}/bower_components/select2/dist/css/select2.min.css">
@stop

@section('content')
	<section class="create-student-section">
		<div class="box box-info">
			<div class="box-header with-border">
				{{ $title }}
			</div>
			<div class="box-body">
				<form role="form" enctype="multipart/form-data" action="{{ isset($student) ? url('save-student') : url('store-student') }}" method="post">
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						<label>
							Name
						</label>
						{{ csrf_field() }}
						@if (isset($student))
							<input type="hidden" name="id" value="{{ $student->id }}">
						@endif
						<input type="text" class="form-control" required="" name="name" value="{{ isset($student) ?  $student->name : old('name') }}">
						@if ($errors->has('name'))
							<span class='help-block'>
								{{ $errors->first('name') }}
							</span>
						@endif
					</div>
					<div class="form-group {{ $errors->has('serial_number') ? 'has-error' : '' }}">
						<label>
							Serial Number
						</label>
						<input type="text" class="form-control" required="" name="serial_number" value="{{ isset($student) ? $student->serial_number : old('serial_number') }}">
						@if ($errors->has('serial_number'))
							<span class='help-block'>
								{{ $errors->first('serial_number') }}
							</span>
						@endif
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-sm btn-info pull-right">{{ isset($student) ? 'Save' : 'Create' }}</button>
					</div>
				</form>
			</div>
		</div>
	</section>
@stop

@section('script')
	<script type="text/javascript" src="{{ url('admin') }}/plugins/bootsnipp-file-input/bootsnipp-file-input.js"></script>
	<!-- Select2 -->
	<script src="{{ url('admin') }}/bower_components/select2/dist/js/select2.full.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.select2').select2();
		});
	</script>
@stop