@extends('layouts.homeLayout')

@section('content')
    
        <!-- breadcumb-area start -->
    <div class="breadcumb-area bg-1">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="breadcumb-wrap text-center">
              <h2>Login</h2>
              <ul>
                <li><a href="{{ url('') }}">Astone</a></li>
                <li><i class="fa fa-angle-right"></i></li>
                <li>Login</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <section class="mt-20 mb20 login">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-push-3">
            <form name="login-form" method="post" class="clearfix" action="{{ route('login') }}">
              <div class="row">
                <div class="form-group col-md-12 {{ $errors->has('email') ? 'has-error' : '' }}">
                  <label for="form_username_email">User Name</label>
                    {{ csrf_field() }}
                  <input id="form_username_email" name="email" value="{{ old('email') }}" class="form-control" type="text" required autofocus>
                  @if ($errors->has('email'))
                      <span class="help-block">
                        {{ $errors->first('email') }}
                      </span>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12 {{ $errors->has('password') ? 'has-error' : '' }}">
                  <label for="form_password">Password</label>
                  <input id="form_password" name="password" class="form-control" type="password">
                  @if ($errors->has('password'))
                      <span class="help-block">
                        {{ $errors->first('password') }}
                      </span>
                  @endif
                </div>
              </div>
              <div class="checkbox pull-right mt-15">
                <label for="form_checkbox">
                  <input id="form_checkbox" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                  Remmember Me </label>
              </div>
              <div class="form-group pull-left">
                <button type="submit" class="submit-button">Login</button>
              </div>
              <div class="clear text-center pt-16">
                <a class="text-theme-colored font-weight-600 font-12" href="{{ route('register') }}">Create new account</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
@endsection