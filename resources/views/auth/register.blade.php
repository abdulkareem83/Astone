@extends('layouts.homeLayout')

@section('content')
        <!-- breadcumb-area start -->
    <div class="breadcumb-area bg-1">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="breadcumb-wrap text-center">
              <h2>Register</h2>
              <ul>
                <li><a href="{{ url('') }}">Astone</a></li>
                <li><i class="fa fa-angle-right"></i></li>
                <li>Register</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="register-section pt-50 mb-50 register">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-md-push-2">
              <form name="reg-form" class="register-form" method="post" action="{{ route('register') }}">
                <div class="row">
                  <div class="form-group col-md-6 {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">
                      Name</label>
                    {{ csrf_field() }}
                    <input name="name" class="form-control" type="text" value="{{ old('name') }}" required>
                    @if( $errors->has('name') )
                        <span class="help-block">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                  </div>
                  <div class="form-group col-md-6 {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label for="email">User Name</label>
                    <input id="email" name="email" class="form-control" type="text" required>
                    @if ($errors->has('email'))
                        <span class='help-block'>
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="password">Password</label>
                    <input id="password" name="password" class="form-control" type="password" required>
                    @if ($errors->has('password'))
                        <span class='help-block'>
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                  </div>
                  <div class="form-group col-md-6 {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label>Confirm Password</label>
                    <input id="password_confirmation" name="password_confirmation"  class="form-control" type="password" required>
                    @if ($errors->has('password'))
                        <span class='help-block'>
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <button class="submit-button btn-block mt-15" type="submit">
                    Register Now!
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
@endsection
