<?php

namespace App\Models;
use URL;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    //
    public function getAbsolPicturePath()
    {
    	return substr($this->picture, strpos($this->picture, 'uploads'));
    }

    public function getPictureAttribute($picture)
    {
        return $picture ? URL::to('uploads/'.$picture) : "";
    }


    public function getDetailsPictureAttribute($detailsPicture)
    {
        return $detailsPicture ? URL::to('uploads/'.$detailsPicture) : '';
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Student', 'student_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function course()
    {
    	return $this->belongsTo('App\Models\Course', 'course_id', 'id');
    }
}
