<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Course;
use App\Models\Student;
use App\Models\Certificate;
use Illuminate\Http\Request;

class HomeCtrl extends Controller
{
    //

    private $_featuredCourses;
    private $_featuredDiplomas;
    
    public function __construct(){
        $this->_featuredCourses = $this->_getFeaturedCourses();
        $this->_featuredDiplomas = $this->_getFeaturedDiplomas();
    }

    /**
     * index. To show home page
     *
     * @return \Illuminate\Http\Response
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Smart Applications Co. <www.smartapps-ye.com>
     */
    public function index(Request $request)
    {
        $indexPage = true;
        $title = "Astone Page";
        $items = [ "diplomas" => [], "courses" => [] ];
        $featuredCourses = $this->_featuredCourses;
        $featuredDiplomas = $this->_featuredDiplomas;

        if($request->has("rsn")){
            $request->session()->forget('validSerialNumber');
        }
        if ($student = $this->_validateSerialNumber($request)){
            $request->session()->put('validSerialNumber', true);
            $request->session()->put('sn', $request->sn);
            $items["diplomas"] = $this->_getdiplomasCertificates($student->id);
            $items["courses"] = $this->_getCoursesCertificates($student->id);
        }

        return view('home.index')
                    ->with(compact('title', 'indexPage', 'items', 'featuredCourses', 'featuredDiplomas'));
    }

    /**
     * index. To show Contact page
     *
     * @return \Illuminate\Http\Response
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Smart Applications Co. <www.smartapps-ye.com>
     */
    public function showContactPage()
    {
        $title = "Contact Page";
        $featuredCourses = $this->_featuredCourses;
        $featuredDiplomas = $this->_featuredDiplomas;

        return view('home.contact')
                    ->with(compact('title', 'featuredCourses', 'featuredDiplomas'));
    }

    /**
     * index. To show Courses page
     *
     * @return \Illuminate\Http\Response
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Smart Applications Co. <www.smartapps-ye.com>
     */
    public function showCoursesPage()
    {
        $title = "Courses Page";
        $featuredCourses = $this->_featuredCourses;
        $featuredDiplomas = $this->_featuredDiplomas;
        $courses = Course::where('type', 'course')->get();

        return view('home.courses')
                    ->with(compact('title', 'courses', 'featuredCourses', 'featuredDiplomas'));
    }

    /**
     * showDiplomasPage. To show diploams page
     *
     * @return \Illuminate\Http\Response
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Smart Applications Co. <www.smartapps-ye.com>
     */
    public function showDiplomasPage()
    {
        $title = "Diplomas Page";
        $featuredCourses = $this->_featuredCourses;
        $featuredDiplomas = $this->_featuredDiplomas;
        $courses = Course::where('type', 'diploma')->get();

        return view('home.courses')
                    ->with(compact('title', 'courses', 'featuredCourses', 'featuredDiplomas'));
    }

 

    private function _validateSerialNumber($request){
        if ($request->has('sn') || $request->session()->get('sn')){
            $serialNumber = $request->has('sn') ? $request->sn : $request->session()->get('sn');
            $student = Student::where("serial_number", $serialNumber)->first();
            if($student != null) return $student;
            $request->session()->flash('invalidSN', 'Invalid National Id');
        }
        return false;
    }

    private function _getCoursesCertificates($studentId){
        return $this->_toAssArray($this->_getCertificates($studentId)->where("courses.type", "course")->get());
    }

    private function _getdiplomasCertificates($studentId){
        return $this->_toAssArray($this->_getCertificates($studentId)->where("courses.type", "diploma")->get());
    }
    private function _getCertificates($studentId){
        return Certificate::join("courses", "courses.id", "=", "certificates.course_id")
                ->select("courses.name", "courses.description", "certificates.id as certificateId", "certificates.picture", "certificates.details_picture")
                ->where("certificates.student_id", $studentId);
    }

    private function _getFeaturedCourses(){
        return Course::where("featured", 1)
                ->where("type", "course")
                ->limit(4)->get();
    }

    private function _getFeaturedDiplomas(){
        return Course::where("featured", 1)
                ->where("type", "diploma")
                ->limit(4)->get();
    }

    private function _toAssArray($arr){
        $newArr = [];
        foreach ($arr as $value) {
            $value->description = str_replace(["\r", "\n"], ["\\r", "\\n"], $value->description);
            $newArr["course{$value->certificateId}"] = $value;
        }
        return $newArr;
    }
}
