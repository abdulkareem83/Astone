<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use App\Models\Student;
use App\Models\Course;
use App\Models\Certificate;
use Illuminate\Http\Request;
use League\Flysystem\FileNotFoundException;
use Exception;

class CertificateCtrl extends Controller
{
    //
    private $mainTitle = "Certificates";

    /**
     * index. To show all certificates page
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function index()
    {
    	$mainTitle = $this->mainTitle;
    	$title = "All Certificate";
    	$certificates = Certificate::all();

    	return view('admin.certificates.index')
    			->with(compact('mainTitle', 'title', 'certificates'));
    }
    
    /**
     * createCertificate. To show create certificate page
     *
     * @return \Illuminate\Http\Response
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function createCertificate()
    {
    	$mainTitle = $this->mainTitle;
    	$title = "Add new certificate";
        $students = Student::all();
        $courses = Course::all(); 
    	
        return view('admin.certificates.create')
    		->with(compact('mainTitle', 'title', 'courses', 'students'));
    }

    /**
     * storeCertificate. To store the certificate
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function storeCertificate(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    			'name'	=> 'required|max:255',
                'student_id' => 'required|exists:students,id',
    			'course_id' => 'required|exists:courses,id',
    			'picture'	=> 'required|image',
    		]);
        if($validator->fails()){
            return redirect()->back()
                    ->withInput()
                    ->withErrors($validator);
        }else{
    		$this->_storeCertificate($request);
    		return redirect('certificates/all')
    				->with('success', 'missionCompleted');
    	}
    }
    
    /**
     * editCertificate. To show edit certificate page
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function editCertificate($id, Request $request)
    {
    	$mainTitle = $this->mainTitle;
    	$title = "Edit certificate";
        $certificate = Certificate::find($id);
    	if( is_null($certificate) ){
    		return redirect('/');
    	}
        $students = Student::all();
        $courses = Course::all();

    	return view('admin.certificates.create')
    			->with(compact('mainTitle', 'title', 'certificate', 'students', 'courses'));
    }
    
    /**
     * saveCertificate. To save the edited certificate
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function saveCertificate(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    			'name'	=> 'required|max:255',
    			'picture' => 'image',
                'students_id' => 'required|exists:studentss,id',
                'course_id' => 'required|exists:courses,id',
    			'id'	=> 'required|exists:certificates,id'
    		]);
    	if($validator->fails()){
    		return redirect()->back()
    				->withInput()
    				->withErrors($validator);
    	}else{
    		$this->_storeCertificate($request);
    		return redirect()->back()
    				->with('success', 'missionCompleted');
    	}
    }

    /**
     * deleteCertificate. To delete a certificate from database
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function deleteCertificate(Request $request)
    {
    	if ($request->has('id') && $request->has('token')) {
    	    $certificate = Certificate::where('id', '=', $request->id)->first();
    	    if ($request->token == session('_token') && $certificate != null) {
    	    	$certificate->delete();
    	        return redirect()->back()
    	                       ->with('success', 'missionCompleted');
    	    }
    	}
    }
        	
    /**
     * downloadCertificate. To download the certificate
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Smart Applications Co. <www.smartapps-ye.com>
     */
    public function downloadCertificate($id)
    {
        $certificate = Certificate::find($id);
        if ($certificate == null || !file_exists($certificate->getAbsolPicturePath()))
            abort(404);

        return response()->download($certificate->getAbsolPicturePath());
    }


    /**
     * _storeCertificate. To store certificate into database
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    private function _storeCertificate($request)
    {
    	if( $request->has('id') ){
    		$certificate = Certificate::find($request->id);
    	}else{
    		$certificate = new Certificate();
    	}
        if ($request->hasFile('picture')) {
            $certificate->picture = uploadFile($request->picture);
        }

    	if ($request->hasFile('details_picture')) {
	    	$certificate->details_picture = uploadFile($request->details_picture);
    	}

    	$certificate->name = $request->name;
        $certificate->student_id = $request->student_id;
    	$certificate->course_id = $request->course_id;
        $certificate->save();
    }
    
}
