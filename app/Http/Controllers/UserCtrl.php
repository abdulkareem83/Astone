<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;

class UserCtrl extends Controller
{
    //
    private $mainTitle = "Users";
    public function __constructor(){

    }

    public function index(){
    	$mainTitle = $this->mainTitle;
    	$title = "All users";
    	$users = User::all();
    	return view('admin.user.index')
    			->with(compact('users', 'mainTitle', 'title'));
    }

    public function editUser($id){
    	$mainTitle = $this->mainTitle;
    	$title = "Edit User";
    	$user = User::find($id);
    	if($user == null)
    		abort(404);
    	return view('admin.user.edit')
    			->with(compact('user', 'mainTitle', 'title'));
    }

    public function saveUser(Request $request){
    	$validator = Validator::make($request->all(), [
    		'id' => 'required|exists:users,id',
            'password' => 'min:|confirmed',
    		'role' => 'required|in:student,admin',
    	]);

    	if($validator->fails()){
    		return redirect()->back()
    				->withInput()
    				->withErrors($validator);
    	}else{
    		$user = User::find($request->id);
    		if($request->has('password')){
                $user->password = bcrypt($request->password);
            }
            $user->role = $request->role;
            $user->save();
    		return redirect('users/all');
    	}
    }

    /**
     * deleteUser. To delete a user from database
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     */
    public function deleteUser(Request $request)
    {
    	if ($request->has('id') && $request->has('token')) {
    	    $user = User::where('id', '=', $request->id)
    	    			->first();
    	    if ($request->token == session('_token') && $user != null) {
    	    	$user->delete();
    	        return redirect()->back()
    	                       ->with('success', 'missionCompleted');
    	    }
    	}
    }
}
