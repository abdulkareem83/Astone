<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use App\Models\Course;
use App\Models\Category;
use Illuminate\Http\Request;

class DiplomaCtrl extends Controller
{
    //
    private $mainTitle;

    function __construct()
    {
    	$this->mainTitle = "Diplomas";
    }
    /**
     * index. To show all diplomas page
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function index()
    {
    	$mainTitle = $this->mainTitle;
    	$title = "All Diplomas";
    	$diplomas = Course::where("type", "diploma")->get();

    	return view('admin.diplomas.index')
    			->with(compact('mainTitle', 'title', 'diplomas'));
    }
    
    /**
     * createDiploma. To show create diploma page
     *
     * @return \Illuminate\Http\Response
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function createDiploma()
    {
    	$mainTitle = $this->mainTitle;
    	$title = "Add new diploma";

    	return view('admin.diplomas.create')
    		->with(compact('mainTitle', 'title'));
    }

    /**
     * storeDiploma. To store the course
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function storeDiploma(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    			'name'	=> 'required|max:255',
    			'description' => 'required',
    			'picture'	=> 'required|image',
    		]);
        if($validator->fails()){
            return redirect()->back()
                    ->withInput()
                    ->withErrors($validator);
        }else{
    		$this->_storeDiploma($request);
    		return redirect('diplomas/all')
    				->with('success', 'missionCompleted');
    	}
    }
    
    /**
     * editDiploma. To show edit diploma page
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function editDiploma($id, Request $request)
    {
    	$mainTitle = $this->mainTitle;
    	$title = "Edit Diploma";
        $diploma = Course::where('type', 'diploma')->find($id);
    	if( is_null($diploma) ){
    		return redirect('/');
    	}
    	return view('admin.diplomas.create')
    			->with(compact('mainTitle', 'title', 'diploma'));
    }
    
    /**
     * saveDiploma. To save the edited diploma
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function saveDiploma(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    			'name'	=> 'required|max:255',
    			'description' => 'required',
    			'picture' => 'image',
    			'id'	=> 'required|exists:courses,id'
    		]);
    	if($validator->fails()){
    		return redirect()->back()
    				->withInput()
    				->withErrors($validator);
    	}else{
    		$this->_storeDiploma($request);
    		return redirect()->back()
    				->with('success', 'missionCompleted');
    	}
    }

    /**
     * deleteDiploma. To delete a diploma from database
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    public function deleteDiploma(Request $request)
    {
    	if ($request->has('id') && $request->has('token')) {
    	    $diploma = Course::where('type', 'diploma')->where('id', '=', $request->id)->first();
    	    if ($request->token == session('_token') && $diploma != null) {
    	    	$diploma->delete();
    	        return redirect()->back()
    	                       ->with('success', 'missionCompleted');
    	    }
    	}
    }


    /**
     * _storeDiploma. To store diploma into database
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Hamdy Soltan.
     */
    private function _storeDiploma($request)
    {
    	if( $request->has('id') ){
    		$diploma = Course::where('type', 'diploma')->find($request->id);
    	}else{
    		$diploma = new Course();
    	}
    	if ($request->hasFile('picture')) {
	    	$diploma->picture = uploadFile($request->picture);
    	}

    	$diploma->name = $request->name;
    	$diploma->description = $request->description;
        $diploma->featured = $request->input('featured') ? 1 : 0;
        $diploma->type = "diploma";
        $diploma->save();
    }
    
}
