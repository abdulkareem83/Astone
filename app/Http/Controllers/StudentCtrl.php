<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use App\Models\Student;
use App\Models\Category;
use App\Models\Certificate;
use Illuminate\Http\Request;

class StudentCtrl extends Controller
{
    //
    private $mainTitle;

    function __construct()
    {
    	$this->mainTitle = "Students";
    }
    /**
     * index. To show all students page
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Astone.
     */
    public function index()
    {
    	$mainTitle = $this->mainTitle;
    	$title = "All Students";
    	$students = Student::all();

    	return view('admin.students.index')
    			->with(compact('mainTitle', 'title', 'students'));
    }
    
    /**
     * createStudent. To show create student page
     *
     * @return \Illuminate\Http\Response
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Astone.
     */
    public function createStudent()
    {
    	$mainTitle = $this->mainTitle;
    	$title = "Add new student";

    	return view('admin.students.create')
    		->with(compact('mainTitle', 'title'));
    }

    /**
     * storeStudent. To store the student
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Astone.
     */
    public function storeStudent(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    			'name'	=> 'required|max:255',
    			'serial_number' => 'required'
    		]);
        if($validator->fails()){
            return redirect()->back()
                    ->withInput()
                    ->withErrors($validator);
        }else{
    		$this->_storeStudent($request);
    		return redirect('students/all')
    				->with('success', 'missionCompleted');
    	}
    }
    
    /**
     * editStudent. To show edit student page
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Astone.
     */
    public function editStudent($id, Request $request)
    {
    	$mainTitle = $this->mainTitle;
    	$title = "Edit Student ";
        $student = Student::find($id);
    	if( is_null($student) ){
    		return redirect('/');
    	}
    	return view('admin.students.create')
    			->with(compact('mainTitle', 'title', 'student'));
    }
    
    /**
     * saveStudent. To save the edited student
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Astone.
     */
    public function saveStudent(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    			'name'	=> 'required|max:255',
    			'serial_number' => 'required',
    			'id'	=> 'required|exists:students,id'
    		]);
    	if($validator->fails()){
    		return redirect()->back()
    				->withInput()
    				->withErrors($validator);
    	}else{
    		$this->_storeStudent($request);
    		return redirect()->back()
    				->with('success', 'missionCompleted');
    	}
    }

    /**
     * deleteStudent. To delete a student from database
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Astone.
     */
    public function deleteStudent(Request $request)
    {
    	if ($request->has('id') && $request->has('token')) {
    	    $student = Student::where('id', '=', $request->id)->first();
    	    if ($request->token == session('_token') && $student != null) {
    	    	$student->delete();
    	        Certificate::where("student_id", $request->id)->delete();
                return redirect()->back()
    	                       ->with('success', 'missionCompleted');
    	    }
    	}
    }
    	
    /**
     * showStudent. To show the student
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Smart Applications Co. <www.smartapps-ye.com>
     */
    public function showStudent($id)
    {
    	$mainTitle = $this->mainTitle;
    	$student = Student::find($id);

    	if (is_null($student)) {
    		return redirect('/');
    	}
    	$title = $student->name;
    	return view('admin.students.show')
    			->with(compact('mainTitle', 'title', 'student'));
    }


    /**
     * _storeStudent. To store student into database
     *
     * @param 
     * @return 
     * @author Abdulkareem Mohammed <a.esawy.sapps@gmail.com>
     * @copyright Astone.
     */
    private function _storeStudent($request)
    {
    	if( $request->has('id') ){
    		$student = Student::find($request->id);
    	}else{
    		$student = new Student();
    	}

    	$student->name = $request->name;
    	$student->serial_number = $request->serial_number;
        $student->save();
    }
    
}
